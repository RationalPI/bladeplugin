// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#include "BladePlugin_ExtrusionToSurface.h"

#include "ModelAPI_AttributeSelectionList.h"
#include "ModelAPI_AttributeSelection.h"
#include "ModelAPI_AttributeDouble.h"
#include "ModelAPI_AttributeBoolean.h"
#include "ModelAPI_ResultBody.h"
#include <ModelAPI_Session.h>
#include <ModelAPI_Validator.h>
#include <GeomAlgoAPI_Filling.h>
#include <GeomAlgoAPI_Copy.h>
#include <GeomAlgoAPI_Sewing.h>
#include "BladePlugin_Utils.h"
#include <GeomAPI_Trsf.h>
#include <GeomAlgoAPI_Transform.h>

#include <iostream>

void BladePlugin_ExtrusionToSurface::initAttributes(){
	data()->addAttribute(AttrBases, ModelAPI_AttributeSelectionList::typeId());
	data()->addAttribute(AttrDirection, ModelAPI_AttributeSelection::typeId());
	data()->addAttribute(Attrlenght, ModelAPI_AttributeDouble::typeId());
	data()->addAttribute(AttrlenghtBis, ModelAPI_AttributeDouble::typeId());
	data()->addAttribute(AttrReverseDirection, ModelAPI_AttributeBoolean::typeId());

	ModelAPI_Session::get()->validators()->registerNotObligatory(getKind(), AttrDirection);
}



void BladePlugin_ExtrusionToSurface::execute(){
	auto getDirection=[this](){
		auto extrusionDirection=directionFromLine(SelToShape(selection(AttrDirection)));
		if(!extrusionDirection){
			auto sketch=SelToSketch(selectionList(AttrBases)->value(0));
			if(sketch)
				extrusionDirection=sketch->norm();
		}
		if(extrusionDirection)
			if(boolean(AttrReverseDirection)->value())extrusionDirection->reverse();

		return extrusionDirection;
	};

	auto extrusionDirection=getDirection();
	if(!extrusionDirection){
		setError("As the first selected item is not a sketch you must select a direction",false);
		return;
	}

	auto lenth=real(Attrlenght)->value();
	auto lenthBis=-real(AttrlenghtBis)->value();
	if(lenth==0.0&&lenthBis==0.0){
		setError("Both lenghts can't be 0",false);
		return;
	}
	if(lenth-lenthBis==0.0){
		setError("Lenghts can't sum to be 0",false);
		return;
	}

	auto translation=std::make_shared<GeomAPI_Trsf>();
	auto translationBis=std::make_shared<GeomAPI_Trsf>();{
		translation->setTranslation(
					extrusionDirection->x()*lenth,
					extrusionDirection->y()*lenth,
					extrusionDirection->z()*lenth
					);
		translationBis->setTranslation(
					extrusionDirection->x()*lenthBis,
					extrusionDirection->y()*lenthBis,
					extrusionDirection->z()*lenthBis
					);
	}

	std::vector<GeomShapePtr> fromSelectionEdges;
	std::vector<std::pair<GeomShapePtr,GeomShapePtr>> oposingEdges;
	for (auto& elem : SelListRange(selectionList(AttrBases))) {
		for (auto& edge : elem->subShapes(GeomAPI_Shape::ShapeType::EDGE)) {
			fromSelectionEdges.push_back(edge);
			oposingEdges.push_back(
						std::make_pair(
							GeomAlgoAPI_Transform(edge,translationBis).shape(),
							GeomAlgoAPI_Transform(edge,translation).shape())
						);
		}
	}

	ListOfShape outputFaces;
	for (auto& edgePair : oposingEdges) {
		auto filling=std::make_shared<GeomAlgoAPI_Filling>();
		filling->add(edgePair.first->edge());
		filling->add(edgePair.second->edge());
		filling->build(false);
		HANDLE_ERROR(filling);
		outputFaces.push_back(filling->shape());
	}

	GeomMakeShapePtr sewingAlgo(new GeomAlgoAPI_Sewing(outputFaces));
	HANDLE_ERROR(sewingAlgo);
	auto sewingResult = sewingAlgo->shape();

	if(sewingResult->subShapes(GeomAPI_Shape::SHELL).size()>1){
		setError("this feature can only work on connected edges or wire",true);
		return;
	}

	int resultIndex = 0;
	ResultBodyPtr resultBody = document()->createBody(data(), resultIndex);
	resultBody->store(sewingResult);

	std::map<GeomShapePtr,std::vector<std::pair<GeomShapePtr,std::string>>> generatedMap;
	std::map<GeomShapePtr,std::vector<std::pair<GeomShapePtr,std::string>>> modified_Map;
	for (auto& edge : sewingResult->subShapes(GeomAPI_Shape::EDGE)){
		size_t i=0;
		for (auto& opoEdges : oposingEdges) {
			if(isComparable(edge->edge(),opoEdges.first->edge())){
				modified_Map[fromSelectionEdges[i]].push_back(std::make_pair(edge,"SRC"));
			}else if(isComparable(edge->edge(),opoEdges.second->edge())){
				generatedMap[fromSelectionEdges[i]].push_back(std::make_pair(edge,"DST"));
			}
			i++;
		}
	}
	size_t i=0;
	for (auto& face : sewingResult->subShapes(GeomAPI_Shape::FACE)){
		generatedMap[fromSelectionEdges[i++]].push_back(std::make_pair(face,"Face"));
	}

	for (auto& gen : modified_Map) {
		for (auto& to : gen.second) {
			resultBody->modified(gen.first,to.first,to.second);
		}
	}
	for (auto& gen : generatedMap) {
		for (auto& to : gen.second) {
			resultBody->generated(gen.first,to.first,to.second);
		}
	}

	setResult(resultBody, resultIndex++);
	removeResults(resultIndex);
}
