// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#include <BladePlugin_Plugin.h>
#include <BladePlugin_ExtrusionToSurface.h>
#include <ModelAPI_Session.h>

// the only created instance of this plugin
static BladePlugin_Plugin* MY_FEATURES_INSTANCE = new BladePlugin_Plugin();

BladePlugin_Plugin::BladePlugin_Plugin(){
  ModelAPI_Session::get()->registerPlugin(this);
}

FeaturePtr BladePlugin_Plugin::createFeature(std::string theFeatureID)
{
  if (theFeatureID == BladePlugin_ExtrusionToSurface::ID) {
    return FeaturePtr(new BladePlugin_ExtrusionToSurface);
  }

  // feature of such kind is not found
  return FeaturePtr();
}
