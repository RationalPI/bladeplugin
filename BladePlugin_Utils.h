// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#pragma once

#include "ModelAPI_AttributeSelection.h"
#include "ModelAPI_AttributeSelectionList.h"
#include <GeomAPI_Shape.h>
#include <GeomAPI_Dir.h>
#include <GeomAPI_PlanarEdges.h>
#include <GeomAlgoAPI_Tools.h>

#include <vector>

#define HANDLE_ERROR(algo)\
{\
    std::string anError;\
    if (GeomAlgoAPI_Tools::AlgoError::isAlgorithmFailed(algo, getKind(), anError))\
{ setError(anError); return; }\
    }\

GeomShapePtr shapeFromSelection(AttributeSelectionPtr selection);

GeomDirPtr directionFromLine(GeomShapePtr shape);

bool isComparable(GeomEdgePtr a,GeomEdgePtr b);;



struct SelToSketch:public std::shared_ptr<GeomAPI_PlanarEdges>{
    SelToSketch(AttributeSelectionPtr selection);
    SelToSketch(){}
};

struct SelListRange:public std::vector<GeomShapePtr>{
    SelListRange(AttributeSelectionListPtr selectionListe);
    SelListRange(){}
};

struct SelToShape:GeomShapePtr{
    SelToShape(AttributeSelectionPtr selection);
    SelToShape(){}
};
