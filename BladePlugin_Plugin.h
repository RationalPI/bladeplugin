// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#pragma once

#include "BladePlugin.h"
#include <ModelAPI_Plugin.h>
#include <ModelAPI_Feature.h>

struct BLADEPLUGIN_EXPORT BladePlugin_Plugin : public ModelAPI_Plugin{
    BladePlugin_Plugin();
    /// Creates the feature object of this plugin by the feature string ID
    virtual FeaturePtr createFeature(std::string theFeatureID);
};
