// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#include "BladePlugin_Utils.h"

#include <GeomAPI_Edge.h>
#include <GeomAPI_Lin.h>
#include <GeomAPI_ShapeIterator.h>

GeomShapePtr shapeFromSelection(AttributeSelectionPtr selection){
    GeomShapePtr aShape = selection->value();
    if (!aShape.get())
        if (selection->context().get())
            aShape = selection->context()->shape();
    return aShape;
}

GeomDirPtr directionFromLine(GeomShapePtr shape){
    GeomEdgePtr dirEdge;
    if (shape.get()) {
        if (shape->isEdge())
            dirEdge = shape->edge();
        else if (shape->isCompound())
            dirEdge = GeomAPI_ShapeIterator(shape).current()->edge();
    }

    GeomDirPtr dirDir;
    if(dirEdge.get())
        if(dirEdge->isLine())
            dirDir = dirEdge->line()->direction();

    return dirDir;
}

SelListRange::SelListRange(AttributeSelectionListPtr selectionListe){
    for (int i = 0; i < selectionListe->size(); ++i) {
        push_back(SelToShape(selectionListe->value(i)));
    }
}

SelToShape::SelToShape(AttributeSelectionPtr selection){
    auto shape = selection->value();
    if(!shape.get()) {
        auto aContext = selection->context();
        if(!aContext.get())
            return;
        shape = aContext->shape();
    }
    this->swap(shape);
}


bool isSketch(GeomShapePtr shape){
    auto maybePlanar=std::dynamic_pointer_cast<GeomAPI_PlanarEdges>(shape);
    return bool(maybePlanar);
}

bool isFromSketch(AttributeSelectionPtr selection){
    return isSketch(selection->context()->shape())||isSketch(SelToShape(selection));
}

SelToSketch::SelToSketch(AttributeSelectionPtr selection){
    auto sketch = std::dynamic_pointer_cast<GeomAPI_PlanarEdges>(selection->value());
    if(!sketch)
        sketch = std::dynamic_pointer_cast<GeomAPI_PlanarEdges>(selection->context()->shape());
    this->swap(sketch);
}

bool isComparable(GeomEdgePtr a, GeomEdgePtr b){
	auto c=a->lastPoint()->isEqual(b->lastPoint());
	auto d=a->firstPoint()->isEqual(b->firstPoint());

	auto e=a->lastPoint()->isEqual(b->firstPoint());
	auto f=a->firstPoint()->isEqual(b->lastPoint());

	return (c&&d)||(e&&f);
}
