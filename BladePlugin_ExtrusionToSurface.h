// Copyright (C) 2019-2019  SALMIERI Nicolas
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// email : 1salmieri.nicolas@gmail.com
//

#pragma once
#include "BladePlugin.h"
#include <ModelAPI_Feature.h>

BLADEPLUGIN_EXPORT struct BladePlugin_ExtrusionToSurface : public ModelAPI_Feature {
    static constexpr auto ID="ExtrusionToSurface";

    static constexpr auto AttrBases="AttrBases";
    static constexpr auto AttrDirection="AttrDirection";
    static constexpr auto Attrlenght="Attrlenght";
	 static constexpr auto AttrlenghtBis="AttrlenghtBis";
    static constexpr auto AttrReverseDirection="AttrReverseDirectionXIOHAOZ";

    virtual const std::string& getKind(){
        static std::string MY_KIND = ID;
        return MY_KIND;
    }

    void initAttributes();

    void execute();

};
